# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    "DB_STRING", 'postgres://postgres:oreoreese14@localhost:5432/bookdb')
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class Book(db.Model):
    __tablename__ = 'books'

    title = db.Column(db.String(254), nullable=False)
    image_url = db.Column(db.String(2000))
    author = db.Column(db.String(80))
    description = db.Column(db.String(10000))
    publisher = db.Column(db.String(80))
    publication_date = db.Column(db.String(254))
    isbn = db.Column(db.String(80))
    book_id =db.Column(db.Integer, primary_key=True)

class Author(db.Model):
    __tablename__ = 'authors'

    name = db.Column(db.String(254))
    image_url = db.Column(db.String(2000))
    books = db.Column(db.String(20000))
    description = db.Column(db.String(10000))
    born = db.Column(db.String(254))
    education = db.Column(db.String(254))
    nationality = db.Column(db.String(254))
    alma_mater = db.Column(db.String(254))
    wikipedia_url = db.Column(db.String(254))
    author_id =db.Column(db.Integer, primary_key=True)

class Publisher(db.Model):
    __tablename__ = 'publishers'

    name = db.Column(db.String(254))
    image_url = db.Column(db.String(2000))
    books = db.Column(db.String(8000))
    description = db.Column(db.String(10000))
    owner = db.Column(db.String(254))
    website = db.Column(db.String(254))
    wikipedia_url = db.Column(db.String(254))
    publisher_id =db.Column(db.Integer, primary_key=True)

db.drop_all()
db.create_all()
# End of models.py
