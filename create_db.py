# beginning of create_db.py
import json
from models import app, db, Book, Publisher, Author

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_books():
    book = load_json('books.json')
    book_id = 0

    for oneBook in book['Books']:
        try:
            title = oneBook['title']
        except:
            title = 'Not in Database'
        try:
            image_url = oneBook['image_url']
        except:
            image_url = 'Not in Database'
        try:
            author = oneBook['authors'][0]['name']
        except:
            author = 'Not in Database'
        try:
            description = oneBook['description']
        except:
            description = 'Not in Database'
        try:
            publisher = oneBook['publishers'][0]['name']
        except:
            publisher = 'Not in Database'
        try:
            publication_date = oneBook['publication_date']
        except:
            publication_date = 'Not in Database'
        try:
            isbn = oneBook['isbn']
        except:
            isbn = 'Not in Database'
        book_id += 1

        newBook = Book(title = title, image_url = image_url, author = author, description = description, publisher = publisher, publication_date = publication_date, isbn = isbn, book_id = book_id)

        # After I create the book, I can then add it to my session.
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()

def create_authors():
    book = load_json('books.json')
    author_id = 0
    authors = set()

    for oneBook in book['Books']:
        for author in oneBook['authors']:
            if (author['name'] not in authors):
                try:
                    name = author['name']
                except:
                    name = 'Not in Database'
                try:
                    image_url = author['image_url']
                except:
                    image_url = 'Not in Database'
                try:
                    books = oneBook['title']
                except:
                    books = 'Not in Database'
                try:
                    description = author['description']
                except:
                    description = 'Not in Database'
                try:
                    born = author['born']
                except:
                    born = 'Not in Database'
                try:
                    education = author['education']
                except:
                    education = 'Not in Database'
                try:
                    nationality = author['nationality']
                except:
                    nationality = 'Not in Database'
                try:
                    alma_mater = author['alma_mater']
                except:
                    alma_mater = 'Not in Database'
                try:
                    wikipedia_url = author['wikipedia_url']
                except:
                    wikipedia_url = 'Not in Database'
                author_id += 1
                authors.add(name)
            elif (author['name'] in authors):
                book_new = oneBook['title']
                author_name = (author['name'])
                author_update = Author.query.filter_by(name = author_name).first()
                book_update = author_update.books + ', ' + book_new
                author_update.books = book_update
                db.session.commit
                continue
            else:
                continue


            newAuthor = Author(name = name, image_url = image_url, books = books, description = description, born = born, education = education, nationality = nationality, alma_mater = alma_mater, wikipedia_url = wikipedia_url, author_id = author_id)

            # After I create the author, I can then add it to my session.
            db.session.add(newAuthor)
            # commit the session to my DB.
            db.session.commit()

def create_publishers():
    book = load_json('books.json')
    publisher_id = 0
    publishers = set()

    for oneBook in book['Books']:
        for publisher in oneBook['publishers']:
            if (publisher['name'] not in publishers):
                try:
                    name = publisher['name']
                except:
                    name = 'Not in Database'
                try:
                    image_url = publisher['image_url']
                except:
                    image_url = 'Not in Database'
                try:
                    books = oneBook['title']
                except:
                    books = 'Not in Database'
                try:
                    description = publisher['description']
                except:
                    description = 'Not in Database'
                try:
                    owner = publisher['owner']
                except:
                    owner = 'Not in Database'
                try:
                    website = publisher['website']
                except:
                    website = 'Not in Database'
                try:
                    wikipedia_url = publisher['wikipedia_url']
                except:
                    wikipedia_url = 'Not in Database'
                publisher_id += 1
                publishers.add(name)

            else:
                continue

            newPublisher = Publisher(name = name, image_url = image_url, books = books, description = description, owner = owner, website = website, wikipedia_url = wikipedia_url, publisher_id = publisher_id)

            # After I create the book, I can then add it to my session.
            db.session.add(newPublisher)
            # commit the session to my DB.
            db.session.commit()

create_books()
create_authors()
create_publishers()
# end of create_db.py
